import ehcf
import standard_encoding


class RsaCryptoSystem:
    p1 = 0
    p2 = 0
    modulus = 0
    phy = 0
    encryption_key = 0
    decryption_key = 0

    def generate_modulus(self):
        self.modulus = self.p1 * self.p2

    def generate_phy(self):
        self.phy = (self.p1 - 1) * (self.p2 - 1)

    def generate_decryption_key(self):
        ehcf_results = ehcf.ehcf(self.encryption_key, self.phy)
        self.decryption_key = ehcf_results[0]

    def set_p1(self, p1):
        self.p1 = p1

    def set_p2(self, p2):
        self.p2 = p2

    def set_modulus(self, n):
        self.modulus = n

    def set_encryption_key(self, e):
        self.encryption_key = e

    def encrypt_message(self, message: str):
        encoded_message = standard_encoding.encode(message)
        cypher_text = str(pow(int(encoded_message), self.encryption_key, self.modulus))

        return cypher_text

    def decrypt_message(self, cypher_text: str):
        encoded_plain_text = str(pow(int(cypher_text), self.decryption_key, self.modulus))

        decoded_text = standard_encoding.decode(encoded_plain_text)
        return decoded_text
