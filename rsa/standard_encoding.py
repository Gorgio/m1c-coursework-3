def encode(content: str):
    return ''.join([
        '0' + str(ord(character)-32) if ord(character)-32 < 10 else str(ord(character)-32)
        for character in content
    ])


def decode(encoded_content: str):
    n = 2
    return ''.join([chr(int(encoded_content[i:i + n])+32) for i in range(0, len(encoded_content), n)])
