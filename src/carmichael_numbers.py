import prime_test

carmichael_numbers = []
for i in range(2, 3000, 1):
    if prime_test.isprime3(i) != prime_test.isprime4(i):
        carmichael_numbers.append(i)

print(carmichael_numbers)
