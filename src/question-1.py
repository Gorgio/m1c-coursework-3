from ehcf import ehcf

n = 2**36
e = 3**10

print(ehcf(e, n))

print((ehcf(e, n)[0])**2 * e)

print(20228322767680359793237401 * e % n)
