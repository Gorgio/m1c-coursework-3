import random


def isprime3(n: int):
    if n <= 0:
        raise ValueError('Please provide a positive integer.')

    if n == 1:
        return False

    if n <= 3:
        return True

    for i in range(10):
        # Do Fermat primality test
        a = random.randint(2, n-2)
        # Use pow function instead of a**(n) % n
        a_mod_n = pow(a, n, n)
        if a_mod_n != a:
            return False
    else:
        return True


def power_of_two_divisor(n: int):
    s = 0
    k = n

    while k % 2 == 0:
        s += 1
        k = k//2

    d = n // (2**s)
    return s, d, n


def miller_witness(a: int, n: int):
    divisors = power_of_two_divisor(n - 1)
    s = divisors[0]
    d = divisors[1]
    # Use pow function instead of x = a ** d % n
    x = pow(a, d, n)
    if x == 1 or x == n-1:
        return False

    while s > 1:
        # Use pow function instead of x = x**2 % n
        x = pow(x, 2, n)
        if x == n-1:
            return False
        s -= 1

    return True


def isprime4(n: int):
    if n <= 0:
        raise ValueError('Please provide a positive integer.')

    if n == 1:
        return False

    if n <= 3:
        return True

    for i in range(10):
        a = random.randint(2, n - 2)
        if miller_witness(a, n):
            return False
    else:
        return True

