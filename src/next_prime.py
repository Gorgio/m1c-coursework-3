import prime_test


def next_prime(n: int):
    if n <= 0:
        raise ValueError('Please provide a positive integer.')

    if n == 1 or n == 2:
        return 2

    next_prime_number = n
    if n % 2 == 0:
        next_prime_number += 1
    else:
        if prime_test.isprime4(n):
            return n

    while not prime_test.isprime3(next_prime_number):
        next_prime_number += 2

    return next_prime_number
