def ehcf(a: int, b: int):
    p1 = 1
    q1 = 0
    h1 = a
    p2 = 0
    q2 = 1
    h2 = b

    while h2 != 0:
        r = h1 // h2
        p3 = p1 - r*p2
        q3 = q1 - r*q2
        h3 = h1 - r*h2
        p1 = p2
        q1 = q2
        h1 = h2
        p2 = p3
        q2 = q3
        h2 = h3

    return p1, q1, h1
