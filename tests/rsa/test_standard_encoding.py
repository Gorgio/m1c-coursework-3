import unittest
import standard_encoding as se


class TestStandardEncoding(unittest.TestCase):
    def test_encoding_decoding(self):
        self.assertEqual('My name is Bond.', se.decode(se.encode('My name is Bond.')))
        self.assertEqual('(This ! special " char $ test % ) # encoding',
                         se.decode(se.encode('(This ! special " char $ test % ) # encoding')))
        self.assertEqual('ABCDEFGHIJKLMNOPQRSTUVWXYZ', se.decode(se.encode('ABCDEFGHIJKLMNOPQRSTUVWXYZ')))
        self.assertEqual('abcdefghijklmnopqrstuvwxyz', se.decode(se.encode('abcdefghijklmnopqrstuvwxyz')))
        self.assertEqual('123456789', se.decode(se.encode('123456789')))
        self.assertEqual('[]\\\'^_`{}|~*+,-_/:;<>=?@', se.decode(se.encode('[]\\\'^_`{}|~*+,-_/:;<>=?@')))

