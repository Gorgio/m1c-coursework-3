import unittest
import rsa_crypto_system
import my_primes


class TestRsaCryptoSystem(unittest.TestCase):
    def test_encryption_and_decryption(self):
        rcs = rsa_crypto_system.RsaCryptoSystem()
        rcs.set_encryption_key(43)
        rcs.set_p1(99158679366511727)
        rcs.set_p2(97369398007206967)
        rcs.generate_modulus()
        rcs.generate_phy()
        rcs.generate_decryption_key()

        plain_text = 'My name is Bond.'

        plain_text_threw_encryption = rcs.decrypt_message(rcs.encrypt_message(plain_text))
        self.assertEqual(plain_text, plain_text_threw_encryption)

    def test_my_primes(self):
        rcs = rsa_crypto_system.RsaCryptoSystem()
        rcs.set_encryption_key(my_primes.MyPrimes.e)
        rcs.set_p1(my_primes.MyPrimes.p1)
        rcs.set_p2(my_primes.MyPrimes.p2)
        rcs.generate_modulus()
        rcs.generate_phy()
        rcs.generate_decryption_key()

        plain_text = 'My name is Bond.'

        plain_text_threw_encryption = rcs.decrypt_message(rcs.encrypt_message(plain_text))
        self.assertEqual(plain_text, plain_text_threw_encryption)

    def test_my_primes_different_cypher(self):
        rcs = rsa_crypto_system.RsaCryptoSystem()
        rcs.set_encryption_key(my_primes.MyPrimes.e)
        rcs.set_p1(my_primes.MyPrimes.p1)
        rcs.set_p2(my_primes.MyPrimes.p2)
        rcs.generate_modulus()
        rcs.generate_phy()
        rcs.generate_decryption_key()

        plain_text = 'This is an extremely important message ~~DO NOT DIE!~~ end%'

        plain_text_threw_encryption = rcs.decrypt_message(rcs.encrypt_message(plain_text))
        self.assertEqual(plain_text, plain_text_threw_encryption)
