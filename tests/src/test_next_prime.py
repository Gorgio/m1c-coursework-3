import next_prime
import unittest


class TestNextPrime(unittest.TestCase):
    def test_non_positive(self):
        with self.assertRaises(ValueError):
            next_prime.next_prime(0)
        with self.assertRaises(ValueError):
            next_prime.next_prime(-1)
        with self.assertRaises(ValueError):
            next_prime.next_prime(-5)

    def test_edge_cases(self):
        self.assertEqual(next_prime.next_prime(1), 2)
        self.assertEqual(next_prime.next_prime(2), 2)
        self.assertEqual(next_prime.next_prime(3), 3)

    def test_single_digit(self):
        self.assertEqual(next_prime.next_prime(4), 5)
        self.assertEqual(next_prime.next_prime(5), 5)
        self.assertEqual(next_prime.next_prime(6), 7)
        self.assertEqual(next_prime.next_prime(7), 7)
        self.assertEqual(next_prime.next_prime(8), 11)
        self.assertEqual(next_prime.next_prime(9), 11)

    def test_small_double_digits(self):
        self.assertEqual(next_prime.next_prime(10), 11)
        self.assertEqual(next_prime.next_prime(11), 11)
        self.assertEqual(next_prime.next_prime(12), 13)
        self.assertEqual(next_prime.next_prime(13), 13)
        self.assertEqual(next_prime.next_prime(14), 17)
        self.assertEqual(next_prime.next_prime(15), 17)
        self.assertEqual(next_prime.next_prime(16), 17)
        self.assertEqual(next_prime.next_prime(17), 17)
        self.assertEqual(next_prime.next_prime(18), 19)
        self.assertEqual(next_prime.next_prime(19), 19)
        self.assertEqual(next_prime.next_prime(20), 23)
        self.assertEqual(next_prime.next_prime(21), 23)
        self.assertEqual(next_prime.next_prime(22), 23)
        self.assertEqual(next_prime.next_prime(23), 23)

    def test_double_digits(self):
        self.assertEqual(next_prime.next_prime(50), 53)
        self.assertEqual(next_prime.next_prime(51), 53)
        self.assertEqual(next_prime.next_prime(55), 59)
        self.assertEqual(next_prime.next_prime(63), 67)
        self.assertEqual(next_prime.next_prime(70), 71)
        self.assertEqual(next_prime.next_prime(74), 79)
        self.assertEqual(next_prime.next_prime(80), 83)

    def test_triple_digits(self):
        self.assertEqual(next_prime.next_prime(445), 449)
        self.assertEqual(next_prime.next_prime(506), 509)
        self.assertEqual(next_prime.next_prime(588), 593)
        self.assertEqual(next_prime.next_prime(644), 647)
        self.assertEqual(next_prime.next_prime(702), 709)
        self.assertEqual(next_prime.next_prime(805), 809)
        self.assertEqual(next_prime.next_prime(994), 997)
