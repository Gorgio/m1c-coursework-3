import unittest
from ehcf import ehcf


class TestEhcf(unittest.TestCase):
    def test_small_integers(self):
        self.assertEqual(ehcf(5, 10)[2], 5)
        self.assertEqual(ehcf(10, 5)[2], 5)
        self.assertEqual(ehcf(18, 8)[2], 2)
        self.assertEqual(ehcf(8, 18)[2], 2)
        self.assertEqual(ehcf(5, 7)[2], 1)
        self.assertEqual(ehcf(7, 5)[2], 1)
        self.assertEqual(ehcf(81, 54)[2], 27)
        self.assertEqual(ehcf(54, 81)[2], 27)
        self.assertEqual(ehcf(45, 75)[2], 15)
        self.assertEqual(ehcf(75, 45)[2], 15)

    def test_large_integers(self):
        self.assertEqual(ehcf(15868, 864)[2], 4)
        self.assertEqual(ehcf(864, 15868)[2], 4)
        self.assertEqual(ehcf(63585, 879426)[2], 9)
        self.assertEqual(ehcf(879426, 63585)[2], 9)
        self.assertEqual(ehcf(63585, 879420)[2], 15)
        self.assertEqual(ehcf(879420, 63585)[2], 15)
